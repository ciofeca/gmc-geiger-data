#!/usr/bin/env ruby

# --- configuration -----------------------------------------------------------

PORT="/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0"
SPEED=57600
TIMEOUT=3.5                             # timeout (seconds) for command completion
DECISECS=8                              # timeout (tenths of seconds) on serial read
PEAKCPS=6                               # define 'peak' this minimum number of clicks per second
MEMSIZE=65536                           # GMC 300E+ only has 64k internal buffer (~1017 minutes max logging)
EXTRAPAGE=1376                          # unmapped bytes at the end, depends on GMC model
CHUNK=500                               # how many bytes to read at every SPIR command

XSIZE=1600                              # x/y sizes in pixels of the output image
YSIZE=900
HIGHCPM=120                             # standard output image height

OUTPUT='/tmp/gmc.png'                   # output graph via gnuplot, saved as png
OUTEXT='/tmp/gmc.dat'                   # output data as tab-separated records
OUTRAW='/tmp/gmc.raw'                   # raw dump output file, empty string for no output

DEBUG=true                              # echo commands/replies on stdout


# --- library -----------------------------------------------------------------

require 'timeout'
require 'termios'
require 'fcntl'

def info str
  STDERR.puts str
end

def debug str
  STDERR.puts(str)  if DEBUG
end


class Time
  def as_timestamp
    strftime '%F.%T'
  end
end

class String

  def as_hex                            # hexify string for screen output
    str = ''
    each_byte do |i|
      str += "%02x " % [i]
    end
    str.strip
  end

  def as_timestamp
    t = unpack 'c6'                     # only consider 6 bytes: YYmmaaHHMMSS
    t[0] += 2000                        # will need a fix after the year 2255
    Time.mktime *t
  end

end


class File                              # add extras for the serial channel

  def self.open_serial port, speed
    # open port device read/write, no tty, binary, sync, non blocking:
    #
    fp = self.open port, File::RDWR|Fcntl::O_NOCTTY|Fcntl::O_NDELAY
    fp.binmode
    fp.sync = true
    fp.fcntl Fcntl::F_SETFL, fp.fcntl(Fcntl::F_GETFL, 0) & ~Fcntl::O_NONBLOCK

    tf = Termios.tcgetattr fp           # set speed and switch to 8/N/1
    tf.iflag = 0
    tf.oflag = 0
    tf.lflag = 0
    tf.cflag = (tf.cflag & ~Termios::CSIZE) | Termios.const_get('CS8')
    tf.cflag &= ~Termios::PARENB
    tf.cflag &= ~Termios::CSTOPB
    tf.ispeed = Termios.const_get("B#{speed}")
    tf.ospeed = Termios.const_get("B#{speed}")

    tf.cc[Termios::VTIME] = DECISECS
    tf.cc[Termios::VMIN] = 0
    tf.cflag |= Termios::CLOCAL | Termios::CREAD

    Termios.tcsetattr fp, Termios::TCSANOW, tf
    Termios.tcflush fp, Termios::TCIOFLUSH
    fp.readpartial 0
    fp
  end

  def cmd command, expectedbytes = 0    # send a command, return the answer
    out = ''.encode('BINARY')
    str = if command[0..3] != 'SPIR'
            "<#{command.upcase}>"
	  else
            "<#{command}>"              # SPIR args won't geat special treatment
	  end

    sleep 0.5                           # a little breath

    begin
      Timeout.timeout(TIMEOUT) do
        debug "!--sending: #{str} and waiting for #{expectedbytes} bytes"  unless str.start_with? '<SPIR'
        self.print str, '>'

        while out.size < expectedbytes
          begin
            got = self.readpartial expectedbytes #-out.size
          rescue EOFError
          end

          #debug "!--#{Time.now.strftime '%T.%L'}  got #{got.size} bytes"  if got
          out += got  if got
        end
      end
      return out                        # if no timeouts, deliver it

    rescue Timeout::Error               # expired timeout?
      debug "!--timeout after reading: #{out.inspect}"
    end

    info "!--timeout error on #{command.inspect}, only read #{out.size}/#{expectedbytes} bytes"
    info(out.as_hex)  unless out.empty?
    exit 1
  end


  def mem start, len                    # baroque process to read+decode device "flash" memory data
    spir = 'SPIR'.force_encoding('binary')
    spir += [ start ].pack('N')[1..-1]  # 24 bit big endian, 0 to MEMSIZE-1
    spir += [ len   ].pack('n')         # 16 bit big endian, 1 to 4096
    #debug("!--sending: SPIR(0x%04x,%d)" % [ start, len ])
    cmd spir, len
  end

end


# --- main --------------------------------------------------------------------

unless ARGV.empty? || ARGV == [ 'alldata' ]
  info "!--usage: #{$0} [ alldata ]"
  exit 2
end

waitmsg = true
while ! File.readable? PORT             # wait until connected (aka: until port appears)
  info("!--waiting: #{PORT}")  if waitmsg
  waitmsg = false
  sleep 3
end

fp = File.open_serial PORT, SPEED
_ = fp.read                             # flush data we didn't request

ver = fp.cmd('getver', 14)
if ver[0..4] != 'GMC-3'
  info "!--read error; reboot device and restart - #{ver.as_hex}"
  exit 3
end
info "!--model:   #{ver}"

bat = fp.cmd('getvolt', 1)[0..0].ord    # assume 3.7V battery (old models have a 9V one)
if bat < 30 || bat > 45                 # whine if outside reasonable values 3.0V-4.5V
  info "!--battery voltage communication error (#{bat}); reboot device and restart"
  debug "!--remaining data: #{fp.read.inspect}"
  exit 4
end
info "!--battery: #{bat/10.0}V"

num = fp.cmd('getserial', 7).as_hex
info "!--serial#: #{num}"

dat = fp.cmd('getdatetime', 7)
if dat[-1..-1] == 0xaa.chr && dat.as_hex[0..0] == '1'
  info "!--date:    #{dat.as_timestamp}"
else
  info "!--cannot get device date/time"
  exit 5
end

cfg = fp.cmd('getcfg', 256)             # only 72 bytes for GMC-300E+
if cfg.nil?
  info "!--cannot get configuration, please reboot the unit"
  exit 6
end
debug "!--config:  #{cfg[0..72].as_hex}"

ignored = fp.mem(MEMSIZE, EXTRAPAGE)    # circumvent bizarre SPI-read firmware bug


# --- read the raw 64k data from its circular buffer in the device eeprom -----

debug "!--fetching raw data"
buf, addr = '', 0
while addr < MEMSIZE
  buf += fp.mem addr, CHUNK
  addr += CHUNK
  STDERR.print '.'
  STDERR.flush
end

unless OUTRAW.empty?
  File.open(OUTRAW, 'wb').print buf     # save a copy of the 64k binary buffer
end
info 'OK'


# --- parse raw buffer while sanitizing data and ignoring garbage -------------

db = []                                 # timestamped readings
buf += buf[0..11]                       # added for index safety
ts, firsts, lasts = nil                 # logged timestamp and first/last ones
i = -1                                  # current index

while true                              # loop: extract unordered data
  i += 1
  break  if i >= MEMSIZE
  val = buf[i].ord                      # fetch next byte

  if val != 0x55                        # click count presumed?
    ts = nil  if val == 0xff            # trash timestamp if uninitialized values
    next  unless ts                     # skip useless value if no timestamp

    #debug "!--value #{val} #{ts.as_timestamp}"

    db << [ ts, val ]                   # store reasonably timestamped values
    ts += 1                             # update timestamp to next "EverySecond"
    next
  end
    
  if buf[i+1].ord == 0xaa               # new timestamp incoming?

    if buf[i+9].ord != 0x55 || buf[i+10].ord != 0xaa
      debug "!--garbage: incomplete timestamp at offset #{i}: #{buf[i..i+11].as_hex}"
    else
      begin                               # try to update timestamp from next bytes
        t = buf[i+3..i+8].as_timestamp
        ts = t
        firsts = ts  unless firsts
        lasts = ts  if lasts == nil || ts > lasts
        #debug "!--timesync #{ts.as_timestamp} from offset #{i}"
      rescue
        ts = nil
        debug "!--ignoring invalid timestamp #{buf[i+3..i+8].inspect} at offset #{i}"
      end
    end

    i += 10
    next

  else
    debug "!--ignoring scaring 0x55 CPS reading as it's probably an incomplete timestamp"
    next
  end

  db << [ ts, val ]
  ts += 1
end

info "!--records: #{db.size}"

db.sort!.uniq! { |elem| elem.first }    # sort by timestamp deleting possible duplicates
info "!--records: #{db.size}"


# since we can't erase eeprom data, by default we will only output today's records
# (except if "alldata" option was requested):
#
if ARGV.first != 'alldata'
  t = Time.now
  today = Time.mktime t.year, t.month, t.day
  db.delete_if { |elem| elem.first < today }
end

if db.size == 0
  info "!--no fresh data available"
  info "!--newest record: #{lasts.as_timestamp}"  if lasts
  info "!--oldest record: #{firsts.as_timestamp}"  if firsts
  exit 0
end


# add the CPM field for the last 60 seconds
#
maxcpm, dat = 0, []
db.each_with_index do |rec, i|
  cpm = 0
  while i >= 0 && db[i].first+60 > rec.first
    cpm += db[i].last
    i -= 1
  end
  dat += [ [ rec, cpm ].flatten ]
  maxcpm = cpm  if maxcpm < cpm
end


# --- output some statistics --------------------------------------------------

from = db.first.first
to = db.last.first
info "!--samples: #{db.size} (today)"
info "!--from:    #{from.as_timestamp}"
info "!--to:      #{to.as_timestamp}"

(0..255).each do |i|                    # show frequency of clicks-per-second values
  n = db.map { |r| r.last }.count i
  next  if n == 0
  p = n * 1000 / db.size
  info "!--values:  #{i}:\t#{n}\t#{p/10}.#{p%10}%"
end

total = 0                               # show timestamps sporting more than peak clicks per second
max = db.first.last
db.each do |elem|
  cps = elem.last
  total += cps
  max = cps  if max < cps
  next  unless cps >= PEAKCPS
  info "!--peak:    #{elem.first.as_timestamp} --> #{cps}"
end
info "!--clicks:  #{total}"
info "!--max cpm: #{maxcpm}"


# --- generate the plot -------------------------------------------------------

info "!--creating gnuplot: #{OUTPUT}"
fp = open OUTEXT, 'w'
dat.each do |elem|
  fp.puts "#{elem.first.strftime '%Y%m%d.%H%M%S '} #{elem[1]} #{elem.last}"
end
fp.close

high = HIGHCPM
high = maxcpm*1.05  if high < maxcpm

fp = IO.popen 'gnuplot', 'w'            # dump to a gnuplot image
fp.puts "
set output   '#{OUTPUT}'
set terminal png large size #{XSIZE},#{YSIZE}
set xdata    time
set timefmt  '%Y%m%d.%H%M%S:'
set format x '%H:%M'
set yrange   [-0.5 : 24]
set y2range  [-19 : #{high}]
set y2tics
set encoding utf8
set label    \"#{from.strftime 'from %Y-%m-%d %H:%M:%S'}\"         at '#{db.first.first.strftime '%Y%m%d.%H%M%S:'}', 23.1
set label    \"#{to.strftime 'to   %Y-%m-%d %H:%M:%S'}\"           at '#{db.first.first.strftime '%Y%m%d.%H%M%S:'}', 22.4
set label    \"#{db.size} reads, #{total} clicks, highest: #{max}\" at '#{db.first.first.strftime '%Y%m%d.%H%M%S:'}', 21.7
set grid
plot '#{OUTEXT}' using 1:2 title 'CPS (clicks per second)' with impulse,         \\
     ''          using 1:3 title 'CPM (clicks per minute)' with lines axes x1y2, \\
     50                    title '0.33 uSv/h'              with lines axes x1y2, \\
     100                   title '0.65 uSv/h'              with lines axes x1y2"
